#include "packet_decoder_ffmpeg.h"

packet_decoder_ffmpeg::packet_decoder_ffmpeg() : c(), m_padding()
{
}

packet_decoder_ffmpeg::~packet_decoder_ffmpeg()
{
	ffmpeg_close();
}

void packet_decoder_ffmpeg::ffmpeg_close() {
	if (c != NULL) {
		avcodec_close(c);
		av_free(c);
		c = NULL;
	}
}
void packet_decoder_ffmpeg::ffmpeg_open( AVCodecID codecID, const void * codec_setup_data , size_t codec_setup_length )
{
	ffmpeg_close();

	pfc::array_t<uint8_t> setup_data;
	setup_data.set_count( codec_setup_length + AV_INPUT_BUFFER_PADDING_SIZE );
	setup_data.fill_null();
	if (codec_setup_length > 0) memcpy( setup_data.get_ptr(), codec_setup_data, codec_setup_length );

	AVCodec *codec = avcodec_find_decoder(codecID);
	if (codec == NULL) throw std::runtime_error("ffmpeg: no requested decoder present");

	c = avcodec_alloc_context3(codec);
	if (c == NULL) throw std::runtime_error("ffmpeg: could not allocate context");
	CAVDictionary options;

	c->extradata = setup_data.get_ptr(); c->extradata_size = (int) codec_setup_length;
	c->request_sample_fmt = AV_SAMPLE_FMT_FLT;
		
	if (avcodec_open2( c, codec, &options.m_obj) < 0) {
		throw std::runtime_error("ffmpeg: could not open the decoder");
	}
}

void packet_decoder_ffmpeg::decode( const void *data, t_size bytes, audio_chunk &p_chunk, abort_callback &p_abort )
{
	enum { reqPadding = AV_INPUT_BUFFER_PADDING_SIZE };
	p_abort.check();
	if (this->m_padding < reqPadding) {
		m_paddingBuffer.grow_size( bytes + reqPadding );
		memcpy(m_paddingBuffer.get_ptr(), data, bytes);
		memset(m_paddingBuffer.get_ptr() + bytes, 0, reqPadding);
		data = m_paddingBuffer.get_ptr();
	}
	AVPacket avpkt = {};
	avpkt.data = (uint8_t*) data;
	avpkt.size = (int) bytes;
	int bytesDone = ffmpeg_decode_packet(c, avpkt, p_chunk );
	(void)bytesDone; // suppress warning
	PFC_ASSERT( (size_t) bytesDone == bytes );
}

void packet_decoder_ffmpeg::get_info ( file_info &p_info )
{
	info_set_dts_info(p_info, c);
}

int packet_decoder_ffmpeg::ffmpeg_codec_profile() {
	return c->profile;
}

void packet_decoder_ffmpeg::reset_after_seek() {
	if (c != NULL) avcodec_flush_buffers(c); 
}

void packet_decoder_ffmpeg::analyze_first_frame(const void *p_buffer, t_size p_bytes, abort_callback &p_abort) {
	audio_chunk_impl dummy; decode(p_buffer, p_bytes, dummy, p_abort);
	this->reset_after_seek();
}

t_size packet_decoder_ffmpeg::set_stream_property ( const GUID &p_type, t_size p_param1, const void *p_param2, t_size p_param2size ) {
	if (p_type == packet_decoder::property_bufferpadding) {
		m_padding = p_param1;
		return AV_INPUT_BUFFER_PADDING_SIZE;
	}
	return 0;
}
