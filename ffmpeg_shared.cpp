#include "ffmpeg_shared.h"

inline audio_sample ImportSample(float in) {return in;}
inline audio_sample ImportSample(int16_t in) {return (float) in / (float) 0x8000;}
inline audio_sample ImportSample(int32_t in) {return (float) in / (float) 0x80000000ull;}

template<typename int_t>
static void Interleave(int_t const * inBuffer, uint32_t samples, uint32_t lineSize, uint32_t channels, float * outBuffer) {
	for(uint32_t c = 0; c < channels; ++c) {
		float * outPtr = outBuffer++;
		int_t const * inPtr = inBuffer;
		for(uint32_t s = 0; s < samples; ++s) {
			*outPtr = ImportSample( *inPtr++ );
			outPtr += channels;
		}
		inBuffer = (int_t const*) ( (uint8_t const*) inBuffer + lineSize ) ;
	}
}

template<typename int_t>
static void Interleave2(int_t const * const * inBuffer, uint32_t samples, uint32_t channels, float * outBuffer) {
	for(uint32_t c = 0; c < channels; ++c) {
		float * outPtr = outBuffer++;
		int_t const * inPtr = *inBuffer;
		for(uint32_t s = 0; s < samples; ++s) {
			*outPtr = ImportSample( *inPtr++ );
			outPtr += channels;
		}
		++inBuffer;
	}
}

static void makeAudioChunk(AVCodecContext const * ctx, AVFrame const & frame, audio_chunk & outChunk) {
	unsigned sample_rate = ctx->sample_rate;
	if (! audio_chunk::g_is_valid_sample_rate( sample_rate ) ) {
		PFC_ASSERT(!"Invalid sample rate");
		throw exception_io_data();
	}
	unsigned channels = ctx->channels;
	if (channels == 0) {
		PFC_ASSERT(!"Invalid channel count");
		throw exception_io_data();
	}
	unsigned layout = (unsigned) ctx->channel_layout;
	if (layout == 0) {
		layout = audio_chunk::g_guess_channel_config( channels );
	} else {
		layout = audio_chunk::g_channel_config_from_wfx( layout );
	}

	outChunk.set_channels( channels, layout );
	outChunk.set_sample_rate( sample_rate );
	outChunk.set_sample_count( frame.nb_samples );
	const size_t dataSize = frame.nb_samples * channels ;
	outChunk.set_data_size( dataSize );
	if (frame.nb_samples == 0) return;
	audio_sample * outBuffer = outChunk.get_data();
	switch(ctx->sample_fmt) {
	case AV_SAMPLE_FMT_FLT:
		memcpy(outBuffer, frame.data[0], dataSize * sizeof(float) );
		break;
	case AV_SAMPLE_FMT_S16:
		audio_math::convert_from_int16( (int16_t*) frame.data[0], dataSize, outBuffer, 1.0);
		break;
	case AV_SAMPLE_FMT_S32:
		audio_math::convert_from_int32( (int32_t*) frame.data[0], dataSize, outBuffer, 1.0);
		break;
	case AV_SAMPLE_FMT_FLTP:
		switch(channels) {
		case 1:
			memcpy(outBuffer, frame.data[0], dataSize * sizeof(float) );
			return;
		case 2:
			/*if (haveSSE2 && (frame.nb_samples & 7) == 0) {
				InterleaveFLT32_8word((float*)frame.extended_data[0], (float*)frame.extended_data[1], frame.nb_samples/8, outBuffer);
				return;
			}*/
			break;
			
		}
		Interleave2( (float**) frame.extended_data, frame.nb_samples, channels, outBuffer);
		return;
	case AV_SAMPLE_FMT_S16P:
		Interleave2( (int16_t**) frame.extended_data, frame.nb_samples, channels, outBuffer);
		break;
	case AV_SAMPLE_FMT_S32P:
		Interleave2( (int32_t**) frame.extended_data, frame.nb_samples, channels, outBuffer);
		break;
	default:
		PFC_ASSERT(!"Unknown output format! Implement me!");
		throw std::runtime_error("Unexpected sample format");
	}
}

int ffmpeg_decode_packet(AVCodecContext * ctx, AVPacket & inPacket, audio_chunk & outChunk) {
	AVFrame frame = {};
	int got_frame = 0;
	int rv = avcodec_decode_audio4(ctx, &frame, &got_frame, &inPacket);
	if (rv < 0) {
#ifdef _DEBUG
		char temp[5];
		int32_t rvm = -rv;
		memcpy(temp, &rvm, 4);
		temp[4] = 0;
		FB2K_console_formatter() << "FFmpeg error: " << rv << " (" << temp << ")";
#endif
		if (rv == AVERROR_INVALIDDATA)
			return -1;
		throw exception_io_data();
		//throw std::runtime_error("ffmpeg: avcodec_decode_audio4 failure");
	}

	PFC_ASSERT( (got_frame != 0) == (frame.nb_samples != 0) );
/*	if (!got_frame) { 
		p_chunk.reset(); return; 
	}
	*/
	// no frame decoded => well return empty but with valid format spec
	//if (got_frame == 0) throw std::runtime_error("ffmpeg: avcodec_decode_audio4 => got_frame is zero");

	makeAudioChunk(ctx, frame, outChunk);
	return rv;
}

void ffmpeg_on_error(int errorCode, const char * what) {
	char msg[256];
	if (av_strerror(errorCode, msg, 256) == 0) {
		throw std::runtime_error( (PFC_string_formatter() << "ffmpeg: " << what << " failure: " << msg).get_ptr() );
	}
	throw std::runtime_error( (PFC_string_formatter() << "ffmpeg: " << what << " failure: error# " << pfc::format_hex((uint32_t)errorCode,8) ).get_ptr() );
}

void ffmpeg_on_retval(int retval, const char * what) {
	if (retval < 0) ffmpeg_on_error(retval, what);
}

#define FFMPEG_DEFAULT_SAMPLE_BUFFER_SIZE 2048
#define FFMPEG_DEFAULT_IO_BUFFER_SIZE 128 * 1024

ffmpeg_codec_data::ffmpeg_codec_data()
{
	start = 0;
	offset = 0;
	size = 0;
	logical_offset = 0;

	channels = 0;
	bitsPerSample = 0;
	floatingPoint = 0;
	sampleRate = 0;
	bitrate = 0;

	totalSamples = 0;
	blockAlign = 0;
	frameSize = 0;
	skipSamples = 0;

	sampleBuffer = 0;
	sampleBufferBlock = 0;

	codec = 0;

	buffer = 0;
	ioCtx = 0;
	streamIndex = -1;
	formatCtx = 0;
	codecCtx = 0;
	lastDecodedFrame = 0;
	lastReadPacket = 0;
	bytesConsumedFromDecodedFrame = 0;
	readNextPacket = 0;
	endOfStream = 0;
	endOfAudio = 0;
	skipSamplesSet = 0;

	samplesToDiscard = 0;
}

static int g_ffmpeg_initialized;
static struct g_init_ffmpeg
{
	g_init_ffmpeg() {
		g_ffmpeg_initialized = 0;
		if (g_ffmpeg_initialized == 1) {
			while (g_ffmpeg_initialized < 2); /* active wait for lack of a better way */
		}
		else if (g_ffmpeg_initialized == 0) {
			g_ffmpeg_initialized = 1;
			av_log_set_flags(AV_LOG_SKIP_REPEATED);
			av_log_set_level(AV_LOG_ERROR);
			//av_register_all(); /* not needed in newer versions */
			g_ffmpeg_initialized = 2;
		}
	}
} _g_init_ffmpeg;

static int ffmpeg_read(void *opaque, uint8_t *buf, int read_size) {
	ffmpeg_codec_data *data = (ffmpeg_codec_data *)opaque;
	int bytes = 0;

	/* clamp reads */
	if (data->logical_offset + read_size > data->size)
		read_size = data->size - data->logical_offset;
	if (read_size == 0)
		return bytes;

	/* main read */
	try
	{
		if (data->streamfile->get_position(*data->abortcallback) != data->offset)
			data->streamfile->seek(data->offset, *data->abortcallback);
		bytes = data->streamfile->read(buf, read_size, *data->abortcallback);
	}
	catch (...)
	{
		bytes = 0;
	}
	data->logical_offset += bytes;
	data->offset += bytes;
	return bytes;
}

/* AVIO callback: write stream not needed */
static int ffmpeg_write(void *opaque, uint8_t *buf, int buf_size) {
	return -1;
}

/* AVIO callback: seek stream, handling custom data */
static int64_t ffmpeg_seek(void *opaque, int64_t offset, int whence) {
	ffmpeg_codec_data *data = (ffmpeg_codec_data *)opaque;
	int ret = 0;

	/* get cache'd size */
	if (whence & AVSEEK_SIZE) {
		return data->size;
	}

	whence &= ~(AVSEEK_SIZE | AVSEEK_FORCE);
	/* find the final offset FFmpeg sees (within fake header + virtual size) */
	switch (whence) {
	case SEEK_SET: /* absolute */
		break;

	case SEEK_CUR: /* relative to current */
		offset += data->logical_offset;
		break;

	case SEEK_END: /* relative to file end (should be negative) */
		offset += data->size;
		break;
	}

	/* clamp offset; fseek does this too */
	if (offset > data->size)
		offset = data->size;
	else if (offset < 0)
		offset = 0;

	/* main seek */
	data->logical_offset = offset;
	data->offset = data->start + offset;
	return ret;
}

static int init_seek(ffmpeg_codec_data * data) {
	int ret, ts_index, found_first = 0;
	int64_t ts = 0; /* seek timestamp */
	int64_t pos = 0; /* data offset */
	int size = 0; /* data size (block align) */
	int distance = 0; /* always 0 ("duration") */

	AVStream * stream = data->formatCtx->streams[data->streamIndex];
	AVPacket * pkt = data->lastReadPacket;

	/* read_seek shouldn't need this index, but direct access to FFmpeg's internals is no good */
	/* if (data->formatCtx->iformat->read_seek || data->formatCtx->iformat->read_seek2)
		return 0; */

		/* A few formats may have a proper index (e.g. CAF/MP4/MPC/ASF/WAV/XWMA/FLAC/MP3), but some don't
		 * work with our custom index (CAF/MPC/MP4) and must skip it. Most formats need flag AVSEEK_FLAG_ANY,
		 * while XWMA (with index 0 not pointing to ts 0) needs AVSEEK_FLAG_BACKWARD to seek properly, but it
		 * makes OGG use the index and seek wrong instead. So for XWMA we forcefully remove the index on it's own meta. */
	ts_index = av_index_search_timestamp(stream, 0, /*AVSEEK_FLAG_BACKWARD |*/ AVSEEK_FLAG_ANY);
	if (ts_index >= 0) {
		goto test_seek;
	}


	/* find the first + second packets to get pos/size */
	while (1) {
		av_packet_unref(pkt);
		ret = av_read_frame(data->formatCtx, pkt);
		if (ret < 0)
			break;
		if (pkt->stream_index != data->streamIndex)
			continue; /* ignore non-selected streams */

		if (!found_first) {
			found_first = 1;
			pos = pkt->pos;
			ts = pkt->dts;
			continue;
		}
		else { /* second found */
			size = pkt->pos - pos; /* coded, pkt->size is decoded size */
			break;
		}
	}
	if (!found_first)
		goto fail;
	/* in rare cases there is only one packet */
	//if (size == 0) size = data_end - pos; /* no easy way to know, ignore (most formats don's need size) */

	/* some formats don't seem to have packet.dts, pretend it's 0 */
	if (ts == INT64_MIN)
		ts = 0;

	/* Some streams start with negative DTS (OGG/OPUS). For Ogg seeking to negative or 0 doesn't seem different.
	 * It does seem seeking before decoding alters a bunch of (inaudible) +-1 lower bytes though.
	 * Output looks correct (encoder delay, num_samples, etc) compared to libvorbis's output. */
	if (ts != 0)
		ts = 0;


	/* add index 0 */
	ret = av_add_index_entry(stream, pos, ts, size, distance, AVINDEX_KEYFRAME);
	if (ret < 0)
		return ret;

test_seek:
	/* seek to 0 test + move back to beginning, since we just consumed packets */
	ret = avformat_seek_file(data->formatCtx, data->streamIndex, ts, ts, ts, AVSEEK_FLAG_ANY);
	if (ret < 0)
		return ret; /* we can't even reset_vgmstream the file */

	avcodec_flush_buffers(data->codecCtx);

	return 0;

fail:
	return -1;
}

void free_ffmpeg(ffmpeg_codec_data *data, abort_callback & p_abort) {
	if (data == NULL)
		return;

	data->abortcallback = &p_abort;

	if (data->lastReadPacket) {
		av_packet_unref(data->lastReadPacket);
		free(data->lastReadPacket);
		data->lastReadPacket = NULL;
	}
	if (data->lastDecodedFrame) {
		av_free(data->lastDecodedFrame);
		data->lastDecodedFrame = NULL;
	}
	if (data->codecCtx) {
		avcodec_close(data->codecCtx);
		avcodec_free_context(&(data->codecCtx));
		data->codecCtx = NULL;
	}
	if (data->formatCtx) {
		avformat_close_input(&(data->formatCtx));
		data->formatCtx = NULL;
	}
	if (data->ioCtx) {
		// buffer passed in is occasionally freed and replaced.
		// the replacement must be freed as well.
		data->buffer = data->ioCtx->buffer;
		av_free(data->ioCtx);
		data->ioCtx = NULL;
	}
	if (data->buffer) {
		av_free(data->buffer);
		data->buffer = NULL;
	}
	if (data->sampleBuffer) {
		av_free(data->sampleBuffer);
		data->sampleBuffer = NULL;
	}
	delete data;
}

ffmpeg_codec_data * init_ffmpeg_offset(file::ptr streamFile, uint64_t start, uint64_t size, abort_callback & p_abort) {
	ffmpeg_codec_data * data;
	int errcode, i;
	int streamIndex, streamCount;

	AVStream *stream;
	AVCodecParameters *codecPar = NULL;
	AVRational tb;

	data = new ffmpeg_codec_data;
	if (!data) return NULL;

	data->streamfile = streamFile;
	data->abortcallback = &p_abort;

	data->start = start;
	data->offset = start;
	data->size = size;
	if (data->size == 0 || data->start + data->size > streamFile->get_size_ex(p_abort)) {
		data->size = streamFile->get_size_ex(p_abort) - data->start;
	}
	data->logical_offset = 0;

	/* setup IO, attempt to autodetect format and gather some info */
	data->buffer = (unsigned char *)av_malloc(FFMPEG_DEFAULT_IO_BUFFER_SIZE);
	if (!data->buffer) goto fail;

	data->ioCtx = avio_alloc_context(data->buffer, FFMPEG_DEFAULT_IO_BUFFER_SIZE, 0, data, ffmpeg_read, ffmpeg_write, ffmpeg_seek);
	if (!data->ioCtx) goto fail;

	data->formatCtx = avformat_alloc_context();
	if (!data->formatCtx) goto fail;

	data->formatCtx->pb = data->ioCtx;

	if ((errcode = avformat_open_input(&data->formatCtx, "", NULL, NULL)) < 0) goto fail; /* autodetect */

	if ((errcode = avformat_find_stream_info(data->formatCtx, NULL)) < 0) goto fail;

	/* find valid audio stream */
	streamIndex = -1;
	streamCount = 0;

	for (i = 0; i < data->formatCtx->nb_streams; ++i) {
		stream = data->formatCtx->streams[i];

		if (stream->codecpar && stream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
			streamCount++;

			/* select Nth audio stream if specified, or first one */
			if (streamIndex < 0) {
				codecPar = stream->codecpar;
				streamIndex = i;
			}
		}

		if (i != streamIndex)
			stream->discard = AVDISCARD_ALL; /* disable demuxing for other streams */
	}
	if (streamCount < 1) goto fail;
	if (streamIndex < 0 || !codecPar) goto fail;

	data->streamIndex = streamIndex;
	stream = data->formatCtx->streams[streamIndex];
	data->streamCount = streamCount;

	/* prepare codec and frame/packet buffers */
	data->codecCtx = avcodec_alloc_context3(NULL);
	if (!data->codecCtx) goto fail;

	if ((errcode = avcodec_parameters_to_context(data->codecCtx, codecPar)) < 0) goto fail;

	//av_codec_set_pkt_timebase(data->codecCtx, stream->time_base); /* deprecated and seemingly not needed */

	data->codec = avcodec_find_decoder(data->codecCtx->codec_id);
	if (!data->codec) goto fail;

	if ((errcode = avcodec_open2(data->codecCtx, data->codec, NULL)) < 0) goto fail;

	data->lastDecodedFrame = av_frame_alloc();
	if (!data->lastDecodedFrame) goto fail;
	av_frame_unref(data->lastDecodedFrame);

	data->lastReadPacket = (AVPacket *)malloc(sizeof(AVPacket));
	if (!data->lastReadPacket) goto fail;
	av_new_packet(data->lastReadPacket, 0);

	data->readNextPacket = 1;
	data->bytesConsumedFromDecodedFrame = INT_MAX;

	/* other setup */
	data->sampleRate = data->codecCtx->sample_rate;
	data->channels = data->codecCtx->channels;
	data->floatingPoint = 0;

	switch (data->codecCtx->sample_fmt) {
	case AV_SAMPLE_FMT_U8:
	case AV_SAMPLE_FMT_U8P:
		data->bitsPerSample = 8;
		break;

	case AV_SAMPLE_FMT_S16:
	case AV_SAMPLE_FMT_S16P:
		data->bitsPerSample = 16;
		break;

	case AV_SAMPLE_FMT_S32:
	case AV_SAMPLE_FMT_S32P:
		data->bitsPerSample = 32;
		break;

	case AV_SAMPLE_FMT_FLT:
	case AV_SAMPLE_FMT_FLTP:
		data->bitsPerSample = 32;
		data->floatingPoint = 1;
		break;

	case AV_SAMPLE_FMT_DBL:
	case AV_SAMPLE_FMT_DBLP:
		data->bitsPerSample = 64;
		data->floatingPoint = 1;
		break;

	default:
		goto fail;
	}

	data->bitrate = (int)(data->codecCtx->bit_rate);
	data->endOfStream = 0;
	data->endOfAudio = 0;

	/* try to guess frames/samples (duration isn't always set) */
	tb.num = 1; tb.den = data->codecCtx->sample_rate;
	data->totalSamples = av_rescale_q(stream->duration, stream->time_base, tb);
	if (data->totalSamples < 0)
		data->totalSamples = 0; /* caller must consider this */

	data->blockAlign = data->codecCtx->block_align;
	data->frameSize = data->codecCtx->frame_size;
	if (data->frameSize == 0) /* some formats don't set frame_size but can get on request, and vice versa */
		data->frameSize = av_get_audio_frame_duration(data->codecCtx, 0);

	/* setup decode buffer */
	data->sampleBufferBlock = FFMPEG_DEFAULT_SAMPLE_BUFFER_SIZE;
	data->sampleBuffer = (uint8_t *)av_malloc(data->sampleBufferBlock * (data->bitsPerSample / 8) * data->channels);
	if (!data->sampleBuffer)
		goto fail;

	/* setup decent seeking for faulty formats */
	errcode = init_seek(data);
	if (errcode < 0) {
		goto fail;
	}

	/* expose start samples to be skipped (encoder delay, usually added by MDCT-based encoders like AAC/MP3/ATRAC3/XMA/etc)
	 * get after init_seek because some demuxers like AAC only fill skip_samples for the first packet */
	if (stream->start_skip_samples) /* samples to skip in the first packet */
		data->skipSamples = stream->start_skip_samples;
	else if (stream->skip_samples) /* samples to skip in any packet (first in this case), used sometimes instead (ex. AAC) */
		data->skipSamples = stream->skip_samples;

	if (data->totalSamples == 0) {
		AVFormatContext *formatCtx = data->formatCtx;
		AVCodecContext *codecCtx = data->codecCtx;
		AVPacket *packet = data->lastReadPacket;
		AVFrame *frame = data->lastDecodedFrame;

		uint64_t totalBytes = 0;
		unsigned int endOfAudio = 0, endOfStream = 0;

		while (!endOfAudio && !endOfStream) {
			if (!endOfStream) {
				/* reset old packet */
				av_packet_unref(packet);

				/* get compressed data from demuxer into packet */
				errcode = av_read_frame(formatCtx, packet);
				if (errcode < 0) {
					if (errcode == AVERROR_EOF) {
						endOfStream = 1; /* no more data, but may still output samples */
					}
					else {
						throw exception_io_data();
					}

					if (formatCtx->pb && formatCtx->pb->error) {
						break;
					}
				}

				if (packet->stream_index != data->streamIndex)
					continue; /* ignore non-selected streams */
			}

			totalBytes += packet->size;

			tb.num = 1; tb.den = data->codecCtx->sample_rate;
			data->totalSamples += av_rescale_q(packet->duration, stream->time_base, tb);
		}

		if (data->totalSamples)
			data->bitrate = (int)((totalBytes * 8 * data->sampleRate) / data->totalSamples);

		reset_ffmpeg(data, p_abort);
	}

	return data;

fail:
	free_ffmpeg(data, p_abort);

	return NULL;
}

void reset_ffmpeg(ffmpeg_codec_data *data, abort_callback &p_abort) {
	if (!data) return;

	data->abortcallback = &p_abort;

	if (data->formatCtx) {
		avformat_seek_file(data->formatCtx, data->streamIndex, 0, 0, 0, AVSEEK_FLAG_ANY);
	}
	if (data->codecCtx) {
		avcodec_flush_buffers(data->codecCtx);
	}
	data->readNextPacket = 1;
	data->bytesConsumedFromDecodedFrame = INT_MAX;
	data->endOfStream = 0;
	data->endOfAudio = 0;
	data->samplesToDiscard = 0;

	/* consider skip samples (encoder delay), if manually set (otherwise let FFmpeg handle it) */
	if (data->skipSamplesSet) {
		AVStream *stream = data->formatCtx->streams[data->streamIndex];
		/* sometimes (ex. AAC) after seeking to the first packet skip_samples is restored, but we want our value */
		stream->skip_samples = 0;
		stream->start_skip_samples = 0;

		data->samplesToDiscard += data->skipSamples;
	}
}

void seek_ffmpeg(ffmpeg_codec_data *data, double seconds, abort_callback &p_abort) {
	int64_t ts;
	if (!data)
		return;

	data->abortcallback = &p_abort;

	/* Start from 0 and discard samples until loop_start (slower but not too noticeable).
	 * Due to various FFmpeg quirks seeking to a sample is erratic in many formats (would need extra steps). */
	data->samplesToDiscard = (uint64_t)(audio_math::time_to_samples(seconds, data->sampleRate));
	ts = 0;

	avformat_seek_file(data->formatCtx, data->streamIndex, ts, ts, ts, AVSEEK_FLAG_ANY);
	avcodec_flush_buffers(data->codecCtx);

	data->readNextPacket = 1;
	data->bytesConsumedFromDecodedFrame = INT_MAX;
	data->endOfStream = 0;
	data->endOfAudio = 0;

	/* consider skip samples (encoder delay), if manually set (otherwise let FFmpeg handle it) */
	if (data->skipSamplesSet) {
		AVStream *stream = data->formatCtx->streams[data->streamIndex];
		/* sometimes (ex. AAC) after seeking to the first packet skip_samples is restored, but we want our value */
		stream->skip_samples = 0;
		stream->start_skip_samples = 0;

		data->samplesToDiscard += data->skipSamples;
	}
}

bool decode_ffmpeg(ffmpeg_codec_data *data, audio_chunk &p_out_chunk, abort_callback &p_abort) {
	if (!data) return false;
	data->abortcallback = &p_abort;

	int samplesReadNow;
	//todo use either channels / data->channels / codecCtx->channels

	AVFormatContext *formatCtx = data->formatCtx;
	AVCodecContext *codecCtx = data->codecCtx;
	AVPacket *packet = data->lastReadPacket;
	AVFrame *frame = data->lastDecodedFrame;
	int planar = av_sample_fmt_is_planar(data->codecCtx->sample_fmt);

	int readNextPacket = data->readNextPacket;
	int endOfStream = data->endOfStream;
	int endOfAudio = data->endOfAudio;
	int bytesConsumedFromDecodedFrame = data->bytesConsumedFromDecodedFrame;

	int bytesPerSample = data->bitsPerSample / 8;
	int bytesRead, bytesToRead;

	int channels = data->channels;

	/* ignore once file is done (but not at endOfStream as FFmpeg can still output samples until endOfAudio) */
	if (/*endOfStream ||*/ endOfAudio) {
		return false;
	}

	int errcode;

	int invalidPackets = 0;

	/* read new data packet when requested */
read_another:
	while (readNextPacket && !endOfAudio) {
		if (!endOfStream) {
			/* reset old packet */
			av_packet_unref(packet);

			/* get compressed data from demuxer into packet */
			errcode = av_read_frame(formatCtx, packet);
			if (errcode < 0) {
				if (errcode == AVERROR_EOF) {
					endOfStream = 1; /* no more data, but may still output samples */
				}
				else {
					throw exception_io_data();
				}

				if (formatCtx->pb && formatCtx->pb->error) {
					break;
				}
			}

			if (packet->stream_index != data->streamIndex)
				continue; /* ignore non-selected streams */
		}

		/* send compressed data to decoder in packet (NULL at EOF to "drain") */

		readNextPacket = 0; /* got compressed data */
	}

	unsigned int packet_size = packet->size;

	errcode = ffmpeg_decode_packet(codecCtx, *packet, p_out_chunk);

	if (errcode < 0) {
		if (++invalidPackets < 16) {
			readNextPacket = 1;
			goto read_another;
		}
		throw exception_io_data();
	}
	unsigned int samples_decoded = 0;
	if (errcode == 0) {
		endOfAudio = 1;
	}
	else if (errcode < packet->size - packet->pos) {
		readNextPacket = 0;
		samples_decoded = p_out_chunk.get_sample_count();
	}
	else {
		readNextPacket = 1;
		samples_decoded = p_out_chunk.get_sample_count();
	}

	if (samples_decoded) {
		data->bitrate = packet_size * 8 * data->sampleRate / samples_decoded;
	}

	/* copy state back */
	data->readNextPacket = readNextPacket;
	data->endOfStream = endOfStream;
	data->endOfAudio = endOfAudio;
	data->bytesConsumedFromDecodedFrame = bytesConsumedFromDecodedFrame;

	return true;
}

void info_set_dts_info(file_info & p_info, ffmpeg_codec_data const * data) {
	p_info.set_length(audio_math::samples_to_time(data->totalSamples, data->sampleRate));
	p_info.info_set_int("bitrate", (int)((data->bitrate + 500) / 1000));
	info_set_dts_info(p_info, data->codecCtx);
}

void info_set_dts_info(file_info & p_info, AVCodecContext const * ctx) {
	if (ctx) {
		if (ctx->codec && ctx->codec->long_name) {
			p_info.info_set("codec", ctx->codec->long_name);
		}
		else if (ctx->codec && ctx->codec->name) {
			p_info.info_set("codec", ctx->codec->name);
		}

		const char *profile = 0;

		switch (ctx->codec->id) {
		case AV_CODEC_ID_DTS:
			switch (ctx->profile) {
			case FF_PROFILE_DTS:
				profile = "DTS";
				break;

			case FF_PROFILE_DTS_ES:
				profile = "DTS-ES";
				break;

			case FF_PROFILE_DTS_96_24:
				profile = "DTS 96/24";
				break;

			case FF_PROFILE_DTS_HD_HRA:
				profile = "DTS-HD HRA";
				break;

			case FF_PROFILE_DTS_HD_MA:
				profile = "DTS-HD MA";
				break;

			case FF_PROFILE_DTS_EXPRESS:
				profile = "DTS Express";
				break;

			default: break;
			}
			break;

		default: break;
		}

		if (ctx->sample_rate > 0) p_info.info_set_int("samplerate", ctx->sample_rate);
		if (ctx->channels > 0) p_info.info_set_int("channels", ctx->channels);
		if (ctx->channel_layout) p_info.info_set_wfx_chanMask((uint32_t)ctx->channel_layout);
		if (profile)
			p_info.info_set("codec_profile", profile);

		bool lossy = true;
		unsigned int bitspersample = 32;

		switch (ctx->sample_fmt) {
		case AV_SAMPLE_FMT_S16:
		case AV_SAMPLE_FMT_S16P:
			bitspersample = 16;
			lossy = false;
			break;

		case AV_SAMPLE_FMT_S32:
		case AV_SAMPLE_FMT_S32P:
			bitspersample = 24;
			lossy = false;
			break;

		case AV_SAMPLE_FMT_FLT:
		case AV_SAMPLE_FMT_FLTP:
			bitspersample = 32;
			break;
		}

		p_info.info_set("encoding", lossy ? "lossy" : "lossless");
		p_info.info_set_int("bitspersample", bitspersample);
	}
}
