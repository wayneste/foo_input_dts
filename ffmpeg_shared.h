#pragma once
#include <foobar2000.h>

extern "C" {
#include <libavutil/avutil.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

class CAVDictionary {
public:
	CAVDictionary() : m_obj() {}

	~CAVDictionary() {
		av_dict_free(&m_obj);
	}

	int set(const char * key, const char * value, int flags) {
		return av_dict_set(&m_obj, key, value, flags);
	}

	AVDictionary * m_obj;
private:
	CAVDictionary(const CAVDictionary&);
	void operator=(const CAVDictionary&);
};

class CAVPacket : public AVPacket {
public:
	CAVPacket() {
		av_init_packet(this);
	}
	~CAVPacket() {
		av_free_packet(this);
	}
private:
	CAVPacket(const CAVPacket&);
	void operator=(const CAVPacket&);
};

typedef struct ffmpeg_codec_data {
	ffmpeg_codec_data();

	/*** IO internals ***/
	file::ptr streamfile;
	abort_callback * abortcallback;

	uint64_t start;             // absolute start within the streamfile
	uint64_t offset;            // absolute offset within the streamfile
	uint64_t size;              // max size within the streamfile
	uint64_t logical_offset;    // computed offset FFmpeg sees

	/*** "public" API (read-only) ***/
	// stream info
	int channels;
	int bitsPerSample;
	int floatingPoint;
	int sampleRate;
	int bitrate;
	// extra info: 0 if unknown or not fixed
	int64_t totalSamples; // estimated count (may not be accurate for some demuxers)
	int64_t blockAlign; // coded block of bytes, counting channels (the block can be joint stereo)
	int64_t frameSize; // decoded samples per block
	int64_t skipSamples; // number of start samples that will be skipped (encoder delay), for looping adjustments
	int streamCount; // number of FFmpeg audio streams

	/*** internal state ***/
	// Intermediate byte buffer
	uint8_t *sampleBuffer;
	// max samples we can held (can be less or more than frameSize)
	size_t sampleBufferBlock;

	// FFmpeg context used for metadata
	AVCodec *codec;

	// FFmpeg decoder state
	unsigned char *buffer;
	AVIOContext *ioCtx;
	int streamIndex;
	AVFormatContext *formatCtx;
	AVCodecContext *codecCtx;
	AVFrame *lastDecodedFrame;
	AVPacket *lastReadPacket;
	int bytesConsumedFromDecodedFrame;
	int readNextPacket;
	int endOfStream;
	int endOfAudio;
	int skipSamplesSet; // flag to know skip samples were manually added from vgmstream

	// Seeking is not ideal, so rollback is necessary
	int samplesToDiscard;
} ffmpeg_codec_data;

// Returns amount of bytes decoded from the packet.
int ffmpeg_decode_packet(AVCodecContext * ctx, AVPacket & inPacket, audio_chunk & outChunk);
void ffmpeg_on_error(int errorCode, const char * what);
void ffmpeg_on_retval(int retval, const char * what);

void info_set_dts_info(file_info & p_info, AVCodecContext const * ctx);

// other stuff
ffmpeg_codec_data * init_ffmpeg_offset(file::ptr streamFile, uint64_t start, uint64_t size, abort_callback & p_abort);
void free_ffmpeg(ffmpeg_codec_data *data, abort_callback & p_abort);

void reset_ffmpeg(ffmpeg_codec_data *data, abort_callback &p_abort);
void seek_ffmpeg(ffmpeg_codec_data *data, double seconds, abort_callback &p_abort);

bool decode_ffmpeg(ffmpeg_codec_data *data, audio_chunk &p_out_chunk, abort_callback &p_abort);

void info_set_dts_info(file_info & p_info, const ffmpeg_codec_data * data);
