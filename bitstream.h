/*
 * This file is part of libdcadec.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef BITSTREAM_H
#define BITSTREAM_H

#include <stdint.h>
#include <limits.h>
#include <stdbool.h>

#include "huffman.h"

static uint32_t DCA_32BE(uint32_t in) {
	const uint8_t * in_ = (const uint8_t *)(&in);
	return (uint32_t)((in_[0]) << 24) |
		(uint32_t)((in_[1]) << 16) |
		(uint32_t)((in_[2]) << 8) |
		(uint32_t)(in_[3]);
}

static uint16_t DCA_16BE(uint16_t in) {
	const uint8_t * in_ = (const uint8_t *)(&in);
	return (uint16_t)((in_[0]) << 8) |
		(uint16_t)(in_[1]);
}

static uint16_t DCA_16LE(uint16_t in) {
	const uint8_t * in_ = (const uint8_t *)(&in);
	return (uint16_t)((in_[1]) << 8) |
		(uint16_t)(in_[0]);
}

static uint16_t dca_bswap16(uint16_t in) {
	return (uint16_t)((in >> 8) & 0xFF) |
		(uint16_t)((in << 8) & 0xFF00);
}

#define BITS_INVALID_VLC_UN  32768
#define BITS_INVALID_VLC_SI -16384

struct bitstream {
    uint32_t    *data;
    int         total;
    int         index;
};

static inline void bits_init(struct bitstream *bits, uint8_t *data, int size)
{
    assert(data && !((uintptr_t)data & 3));
    assert(size > 0 && size < INT_MAX / 8);
    bits->data = (uint32_t *)data;
    bits->total = size * 8;
    bits->index = 0;
}

bool bits_get1(struct bitstream *bits);
int bits_get(struct bitstream *bits, int n);

static inline void bits_skip(struct bitstream *bits, int n)
{
    assert(n >= 0);
    bits->index += n;
}

static inline void bits_skip1(struct bitstream *bits)
{
    bits->index++;
}

#endif
