#include "ffmpeg_shared.h"

#include <stdint.h>

extern "C" {
#include "bitstream.h"
}

enum {
	BUFFER_SIZE = 24576,
	HEADER_SIZE = 16,
	SYNC_SIZE = 4,
	FRAME_SAMPLES = 256
};

// Table 7-1: Sync words
enum SyncWord {
	SYNC_WORD_CORE = 0x7ffe8001,
	SYNC_WORD_CORE_LE = 0xfe7f0180,
	SYNC_WORD_CORE_LE14 = 0xff1f00e8,
	SYNC_WORD_CORE_BE14 = 0x1fffe800,
	SYNC_WORD_REV1AUX = 0x9a1105a0,
	SYNC_WORD_REV2AUX = 0x7004c070,
	SYNC_WORD_XCH = 0x5a5a5a5a,
	SYNC_WORD_XXCH = 0x47004a03,
	SYNC_WORD_X96 = 0x1d95f262,
	SYNC_WORD_XBR = 0x655e315e,
	SYNC_WORD_LBR = 0x0a801921,
	SYNC_WORD_XLL = 0x41a29547,
	SYNC_WORD_EXSS = 0x64582025,
	SYNC_WORD_EXSS_LE = 0x58642520,
	SYNC_WORD_CORE_EXSS = 0x02b09261,
};

#define DCADEC_FRAME_HEADER_SIZE    16

#define DCADEC_BITSTREAM_BE16   0
#define DCADEC_BITSTREAM_LE16   1
#define DCADEC_BITSTREAM_BE14   2
#define DCADEC_BITSTREAM_LE14   3

#define DCADEC_FRAME_TYPE_CORE  0   /**< Backward compatible DTS core */
#define DCADEC_FRAME_TYPE_EXSS  1   /**< Extension sub-stream (EXSS) */

#define SRC_OP(E) \
    uint16_t src_0 = DCA_16##E(_src[0]); \
    uint16_t src_1 = DCA_16##E(_src[1]); \
    uint16_t src_2 = DCA_16##E(_src[2]); \
    uint16_t src_3 = DCA_16##E(_src[3]); \
    uint16_t src_4 = DCA_16##E(_src[4]); \
    uint16_t src_5 = DCA_16##E(_src[5]); \
    uint16_t src_6 = DCA_16##E(_src[6]); \
    uint16_t src_7 = DCA_16##E(_src[7]);

#define DST_OP \
    _dst[0] = DCA_16BE((src_0 <<  2) | ((src_1 & 0x3fff) >> 12)); \
    _dst[1] = DCA_16BE((src_1 <<  4) | ((src_2 & 0x3fff) >> 10)); \
    _dst[2] = DCA_16BE((src_2 <<  6) | ((src_3 & 0x3fff) >>  8)); \
    _dst[3] = DCA_16BE((src_3 <<  8) | ((src_4 & 0x3fff) >>  6)); \
    _dst[4] = DCA_16BE((src_4 << 10) | ((src_5 & 0x3fff) >>  4)); \
    _dst[5] = DCA_16BE((src_5 << 12) | ((src_6 & 0x3fff) >>  2)); \
    _dst[6] = DCA_16BE((src_6 << 14) | ((src_7 & 0x3fff) >>  0));

int dcadec_frame_convert_bitstream(uint8_t *dst, size_t *dst_size,
	const uint8_t *src, size_t src_size)
{
	const uint16_t *_src = (const uint16_t *)src;
	uint16_t *_dst = (uint16_t *)dst;
	size_t count;

	if (!dst || !dst_size || !src || src_size < 4 || ((uintptr_t)_dst & 3))
		return -1;

	if ((uintptr_t)_src & 1)
		_src = (const uint16_t*) memcpy(_dst, _src, src_size);

	switch (DCA_32BE(*(const uint32_t*)_src)) {
	case SYNC_WORD_CORE:
	case SYNC_WORD_EXSS:
		if (_src != _dst)
			memcpy(_dst, _src, src_size);
		*dst_size = src_size;
		return DCADEC_BITSTREAM_BE16;

	case (uint32_t)SYNC_WORD_CORE_LE:
	case SYNC_WORD_EXSS_LE:
		count = (src_size + 1) / 2;
		while (count--)
			*_dst++ = dca_bswap16(*_src++);
		*dst_size = src_size;
		return DCADEC_BITSTREAM_LE16;

	case SYNC_WORD_CORE_BE14:
		count = (src_size + 15) / 16;
		while (count--) {
			SRC_OP(BE)
				DST_OP
				_src += 8;
			_dst += 7;
		}
		*dst_size = src_size - src_size / 8;
		return DCADEC_BITSTREAM_BE14;

	case (uint32_t)SYNC_WORD_CORE_LE14:
		count = (src_size + 15) / 16;
		while (count--) {
			SRC_OP(LE)
				DST_OP
				_src += 8;
			_dst += 7;
		}
		*dst_size = src_size - src_size / 8;
		return DCADEC_BITSTREAM_LE14;

	default:
		return -1;
	}
}

#undef SRC_OP
#undef DST_OP

static int dcadec_frame_parse_header(const uint8_t *data, size_t *size)
{
	struct bitstream bits;
	uint8_t header[DCADEC_FRAME_HEADER_SIZE];
	size_t header_size, frame_size;
	int ret;

	if (!data || !size)
		return -1;

	if ((ret = dcadec_frame_convert_bitstream(header, &header_size,
		data, DCADEC_FRAME_HEADER_SIZE)) < 0)
		return ret;

	bits_init(&bits, header, header_size);

	switch (bits_get(&bits, 32)) {
	case SYNC_WORD_CORE: {
		bool normal_frame = bits_get1(&bits);
		int deficit_samples = bits_get(&bits, 5) + 1;
		int npcmblocks;
		if (normal_frame && deficit_samples != 32)
			return -1;
		bits_skip1(&bits);
		npcmblocks = bits_get(&bits, 7) + 1;
		if ((npcmblocks & 7) && (npcmblocks < 6 || normal_frame))
			return -1;
		frame_size = bits_get(&bits, 14) + 1;
		if (frame_size < 96)
			return -1;
		if (ret & DCADEC_BITSTREAM_BE14)
			*size = frame_size * 8 / 14 * 2;
		else
			*size = frame_size;
		return DCADEC_FRAME_TYPE_CORE;
	}

	case SYNC_WORD_EXSS: {
		bool wide_hdr;
		bits_skip(&bits, 10);
		wide_hdr = bits_get1(&bits);
		header_size = bits_get(&bits, 8 + 4 * wide_hdr) + 1;
		if ((header_size & 3) || header_size < DCADEC_FRAME_HEADER_SIZE)
			return -1;
		frame_size = bits_get(&bits, 16 + 4 * wide_hdr) + 1;
		if ((frame_size & 3) || frame_size < header_size)
			return -1;
		*size = frame_size;
		return DCADEC_FRAME_TYPE_EXSS;
	}

	default:
		return -1;
	}
}

bool is_chunk_silent( audio_chunk * chunk )
{
	audio_sample * data = chunk->get_data();
	for ( unsigned i = 0, j = chunk->get_data_length(); i < j; i++ )
	{
		if ( data[ i ] ) return false;
	}
	return true;
}

class dts_postprocessor_instance : public decode_postprocessor_instance
{
	dsp_chunk_list_impl original_chunks;
	dsp_chunk_list_impl output_chunks;

	AVCodecContext *c;

	pfc::array_t<uint8_t> buffer;
	uint8_t buf[BUFFER_SIZE];
	uint8_t buf_converted[BUFFER_SIZE];
	uint8_t *bufptr, *bufpos;
	bool valid_stream_found;

	int nch, sample_rate, channel_mask;

	size_t frame_size;

	unsigned int contiguous_silent_samples;
	unsigned int contiguous_silent_bytes;
	unsigned int pre_silent_samples;
	unsigned int post_silent_samples;
	unsigned int bytes_skipped;

	uint32_t sync;
	size_t packed_size;

	bool info_emitted, gave_up;

	bool init()
	{
		cleanup();

		pfc::array_t<uint8_t> setup_data;
		setup_data.set_count(AV_INPUT_BUFFER_PADDING_SIZE);
		setup_data.fill_null();

		AVCodec *codec = avcodec_find_decoder(AV_CODEC_ID_DTS);
		if (codec == NULL) throw std::runtime_error("ffmpeg: no requested decoder present");

		c = avcodec_alloc_context3(codec);
		if (c == NULL) throw std::runtime_error("ffmpeg: could not allocate context");
		CAVDictionary options;

		c->extradata = setup_data.get_ptr(); c->extradata_size = 0;
		c->request_sample_fmt = AV_SAMPLE_FMT_FLT;

		if (avcodec_open2(c, codec, &options.m_obj) < 0) {
			throw std::runtime_error("ffmpeg: could not open the decoder");
		}

		return true;
	}

    void cleanup()
    {
		if (c != NULL) {
			avcodec_close(c);
			av_free(c);
			c = NULL;
		}

		original_chunks.remove_all();
		output_chunks.remove_all();

		bufptr = buf;
		bufpos = buf + HEADER_SIZE;
		nch = sample_rate = channel_mask = 0;
		frame_size = 0;
		contiguous_silent_samples = 0;
		contiguous_silent_bytes = 0;
		pre_silent_samples = 0;
		post_silent_samples = 0;
		bytes_skipped = 0;
		valid_stream_found = false;
		info_emitted = false;
		gave_up = false;
		sync = 0;
	}

	bool decode( const void *data, t_size bytes, audio_chunk & p_chunk, unsigned & contiguous_packets, abort_callback & p_abort )
    {
		audio_chunk_impl m_chunk;
		uint8_t *start = (uint8_t *)data;
		uint8_t *end = (uint8_t *)data + bytes;
		unsigned int samples = 0;

		contiguous_packets = 0;

		p_chunk.set_sample_count(0);

		while (1)
		{
			unsigned len = end - start;
			if (!len) break;
			if (len > bufpos - bufptr) len = bufpos - bufptr;

			memcpy(bufptr, start, len);

			bufptr += len;
			start += len;

			if (bufptr == bufpos) {
				if (bufpos == buf + HEADER_SIZE) {
					frame_size = 0;

					sync = DCA_32BE(*(const uint32_t*)buf);

					if (sync == SYNC_WORD_CORE
						|| sync == SYNC_WORD_EXSS
						|| sync == (uint32_t)SYNC_WORD_CORE_LE
						|| sync == SYNC_WORD_EXSS_LE
						|| sync == (uint32_t)SYNC_WORD_CORE_LE14
						|| sync == SYNC_WORD_CORE_BE14) {
						if (dcadec_frame_parse_header(buf, &frame_size) < 0)
							frame_size = 0;
					}

					if (!frame_size) {
						bufptr = buf;
						++bytes_skipped;
						if (!bufptr[0]) {
							if ((++contiguous_silent_bytes & 3) == 0 &&
								(bytes_skipped & 3) == 0) {
								++contiguous_silent_samples;
							}
						}
						else {
							contiguous_silent_bytes = 0;
							contiguous_silent_samples = 0;
						}
						for (bufptr = buf; bufptr < buf + HEADER_SIZE - 1; bufptr++)
							bufptr[0] = bufptr[1];

						continue;
					}
					else
						bytes_skipped += frame_size;

					bufpos = buf + frame_size;
				}
				else
				{
					size_t dst_size = BUFFER_SIZE;
					dcadec_frame_convert_bitstream(buf_converted, &dst_size, buf, frame_size);

					AVPacket avpkt = {};
					avpkt.data = (uint8_t*)buf_converted;
					avpkt.size = (int)dst_size;

					int ret = 0;
					
					try
					{
						ret = ffmpeg_decode_packet(c, avpkt, m_chunk);
					}
					catch (...)
					{
						ret = -1;
					}

					if (ret >= 0) {
						if (ret > 0) {
							if (nch == 0) {
								nch = m_chunk.get_channels();
								channel_mask = m_chunk.get_channel_config();
								sample_rate = m_chunk.get_srate();
							}

							if (!contiguous_packets) {
								p_chunk.copy(m_chunk);
							}
							else {
								PFC_ASSERT(m_chunk.get_channels() == nch);
								PFC_ASSERT(m_chunk.get_srate() == sample_rate);
								p_chunk.grow_data_size((p_chunk.get_sample_count() + m_chunk.get_sample_count()) * nch);
								memcpy(p_chunk.get_data() + p_chunk.get_sample_count() * nch, m_chunk.get_data(), m_chunk.get_sample_count() * nch * sizeof(audio_sample));
								p_chunk.set_sample_count(p_chunk.get_sample_count() + m_chunk.get_sample_count());
							}
						}

						bufptr = buf;
						bufpos = buf + HEADER_SIZE;
						++contiguous_packets;
						continue;
					}
					else {
						bufptr = buf;
						bufpos = buf + HEADER_SIZE;
						contiguous_packets = 0;
						return false;
					}
				}
			}
		}

		return !!p_chunk.get_sample_count();
	}

	unsigned flush_chunks( dsp_chunk_list & p_chunk_list, unsigned insert_point, bool output = false )
	{
		dsp_chunk_list * list = output ? &output_chunks : &original_chunks;
		unsigned ret = list->get_count();
		if ( ret )
		{
			for ( unsigned i = 0; i < list->get_count(); i++ )
			{
				audio_chunk * in = list->get_item( i );
				audio_chunk * out = p_chunk_list.insert_item( insert_point++, in->get_data_length() );
				out->copy( *in );
			}
		}
		original_chunks.remove_all();
		output_chunks.remove_all();
		return ret;
	}

	unsigned flush_silence( dsp_chunk_list & p_chunk_list, unsigned insert_point, unsigned sample_count )
	{
		if ( sample_count )
		{
			audio_chunk * out = p_chunk_list.insert_item( insert_point++, sample_count * nch );
			out->set_srate( sample_rate );
			out->set_channels( nch, channel_mask );
			out->set_silence( sample_count );
			return 1;
		}
		else return 0;
	}

public:
	dts_postprocessor_instance() : c(0)
	{
		cleanup();
	}

	~dts_postprocessor_instance()
	{
		cleanup();
	}

	virtual bool run(dsp_chunk_list & p_chunk_list,t_uint32 p_flags,abort_callback & p_abort)
	{
		if ( gave_up || p_flags & flag_altered ) return false;

		bool modified = false;

		unsigned total_contiguous_packets = 0;

		for ( unsigned i = 0; i < p_chunk_list.get_count(); )
		{
			unsigned contiguous_packets;
			audio_chunk * chunk = p_chunk_list.get_item( i );

			if ( chunk->get_channels() != 2 || ( chunk->get_srate() != 44100 && chunk->get_srate() != 48000 ) ) {
				i += flush_chunks( p_chunk_list, i, valid_stream_found ) + 1;
				continue;
			}

			if (!c)
			{
				if (!init()) break;
			}

			int data = chunk->get_sample_count() * 4;
			buffer.grow_size( data );
			audio_math::convert_to_int16( chunk->get_data(), chunk->get_sample_count() * 2, (t_int16 *)buffer.get_ptr(), 1.0 );

			if ( !valid_stream_found )
			{
				audio_chunk * out = original_chunks.insert_item( original_chunks.get_count(), chunk->get_data_length() );
				out->copy( *chunk );
				if (decode(buffer.get_ptr(), data, *chunk, contiguous_packets, p_abort))
				{
					total_contiguous_packets += contiguous_packets;
					if ( output_chunks.get_count() || total_contiguous_packets >= 2 )
					{
						valid_stream_found = true;
						i += flush_silence( p_chunk_list, i, pre_silent_samples );
						i += flush_chunks( p_chunk_list, i, true ) + 1;
						modified = true;
					}
					else
					{
						out = output_chunks.insert_item( output_chunks.get_count(), chunk->get_data_length() );
						out->copy( *chunk );
						p_chunk_list.remove_by_idx( i );
						pre_silent_samples = contiguous_silent_samples;
					}
				}
				else
				{
					p_chunk_list.remove_by_idx( i );
					output_chunks.remove_all();
				}
			}
			else
			{
				unsigned unused;
				contiguous_silent_samples = 0;
				if ( decode( buffer.get_ptr(), data, *chunk, unused, p_abort ) )
				{
					i++;
					modified = true;
				}
				else
				{
					p_chunk_list.remove_by_idx( i );
				}
				post_silent_samples += contiguous_silent_samples;
			}
		}

		if ( valid_stream_found )
		for ( unsigned i = 0; i < original_chunks.get_count(); )
		{
			audio_chunk * in = original_chunks.get_item( i );
			if ( !is_chunk_silent( in ) ) break;
			flush_silence( p_chunk_list, p_chunk_list.get_count(), in->get_sample_count() );
			original_chunks.remove_by_idx( i );
		}

		if ( original_chunks.get_duration() >= 1.0 )
		{
			flush_chunks( p_chunk_list, p_chunk_list.get_count() );
			gave_up = true;
		}

		if ( p_flags & flag_eof )
		{
			flush_chunks( p_chunk_list, p_chunk_list.get_count(), valid_stream_found );
			flush_silence( p_chunk_list, p_chunk_list.get_count(), post_silent_samples );
			cleanup();
		}

		return modified;
	}

	virtual bool get_dynamic_info( file_info & p_out )
	{
		if ( !info_emitted )
		{
			if ( valid_stream_found )
			{
				info_emitted = true;
				info_set_dts_info(p_out, c);
				return true;
			}
		}
		return false;
	}

	virtual void flush()
	{
		cleanup();
	}

	virtual double get_buffer_ahead()
	{
		return 2.0;
	}
};

class dts_postprocessor_entry : public decode_postprocessor_entry
{
public:
	virtual bool instantiate( const file_info & info, decode_postprocessor_instance::ptr & out )
	{
        if ( info.info_get_decoded_bps() != 16 )
		{
            return false;
        }

		const char * encoding = info.info_get( "encoding" );
		if ( !encoding || pfc::stricmp_ascii( encoding, "lossless" ) )
		{
			return false;
		}

		out = new service_impl_t< dts_postprocessor_instance >;

		return true;
	}
};

static service_factory_single_t< dts_postprocessor_entry > g_dts_postprocessor_entry_factory;
