#include "packet_decoder_ffmpeg.h"

static const int mp4_dts_cs = 169;
static const int mp4_dts_cs_es = 170;

extern int dcadec_frame_convert_bitstream(uint8_t *dst, size_t *dst_size,
	const uint8_t *src, size_t src_size);

class packet_decoder_dts : public packet_decoder_ffmpeg {
	pfc::array_t<uint8_t> m_unpacked_buffer;
public:
    packet_decoder_dts() : packet_decoder_ffmpeg()
    {
    }

    ~packet_decoder_dts()
    {
		ffmpeg_close();
    }

    void open(const GUID &p_owner, bool p_decode, t_size p_param1, const void *p_param2, t_size p_param2size, abort_callback &p_abort)
    {
		ffmpeg_open(AV_CODEC_ID_DTS, NULL, 0);
    }

	static bool g_is_our_setup(const GUID &p_owner, t_size p_param1, const void *p_param2, t_size p_param2size)
    {
        if (p_owner == owner_matroska) {
            if (p_param2size == sizeof(matroska_setup)) {
                const matroska_setup *setup = (const matroska_setup *)p_param2;
                if (!strncmp(setup->codec_id,"A_DTS", 5)) return true; else return false;
            } else return false;
        } else if (p_owner == owner_MP4) {
            if (p_param1 == mp4_dts_cs || p_param1 == mp4_dts_cs_es) {
                return true;
            } else return false;
        }
        else return false;
    }

	void decode(const void *data, t_size bytes, audio_chunk &p_chunk, abort_callback &p_abort) {
		m_unpacked_buffer.grow_size(bytes);
		size_t packet_size;
		if (dcadec_frame_convert_bitstream(m_unpacked_buffer.get_ptr(), &packet_size, (const uint8_t *)data, bytes) < 0)
			throw exception_io_data();
		packet_decoder_ffmpeg::decode(m_unpacked_buffer.get_ptr(), packet_size, p_chunk, p_abort);
	}

    virtual unsigned get_max_frame_dependency() { return 0; }
    virtual double get_max_frame_dependency_time() { return 0; }

    virtual bool analyze_first_frame_supported() { return true; }
};

static packet_decoder_factory_t<packet_decoder_dts> g_packet_decoder_dts_factory;
