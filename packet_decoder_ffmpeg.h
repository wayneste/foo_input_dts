#include "ffmpeg_shared.h"

class packet_decoder_ffmpeg : public packet_decoder
{
	AVCodecContext *c;
	size_t m_padding;
	pfc::array_t<uint8_t> m_paddingBuffer;
public:
	packet_decoder_ffmpeg();
	~packet_decoder_ffmpeg();

	void get_info ( file_info &p_info );
	void ffmpeg_open( AVCodecID codecID, const void * codec_setup_data , size_t codec_setup_length );
	void ffmpeg_close();
	int ffmpeg_codec_profile();
	void decode ( const void *data, t_size bytes, audio_chunk &p_chunk, abort_callback &p_abort );

	t_size set_stream_property ( const GUID &p_type, t_size p_param1, const void *p_param2, t_size p_param2size );
	void reset_after_seek();
	bool analyze_first_frame_supported() { return false; } // OFF by default as not needed for most codecs, but subclasses may override
	void analyze_first_frame(const void *p_buffer, t_size p_bytes, abort_callback &p_abort);
};
