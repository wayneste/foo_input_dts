/*
 * This file is part of libdcadec.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stdint.h>
#include "bitstream.h"

static inline uint32_t bits_peek(struct bitstream *bits)
{
	int pos, shift;
	uint32_t v;

    if (bits->index >= bits->total)
        return 0;

    pos = bits->index >> 5;
    shift = bits->index & 31;

    v = DCA_32BE(bits->data[pos]);
    if (shift) {
        v <<= shift;
        v |= DCA_32BE(bits->data[pos + 1]) >> (32 - shift);
    }

    return v;
}

bool bits_get1(struct bitstream *bits)
{
	uint32_t v;

    if (bits->index >= bits->total)
        return false;

    v = DCA_32BE(bits->data[bits->index >> 5]);
    v <<= bits->index & 31;
    v >>= 32 - 1;

    bits->index++;
    return v;
}

int bits_get(struct bitstream *bits, int n)
{
    uint32_t v = bits_peek(bits);
    v >>= 32 - n;

    bits->index += n;
    return v;
}
